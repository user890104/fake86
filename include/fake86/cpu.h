/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2012 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#define regax 0
#define regcx 1
#define regdx 2
#define regbx 3
#define regsp 4
#define regbp 5
#define regsi 6
#define regdi 7
#define reges 0
#define regcs 1
#define regss 2
#define regds 3

#ifdef __BIG_ENDIAN__
#define regal 1
#define regah 0
#define regcl 3
#define regch 2
#define regdl 5
#define regdh 4
#define regbl 7
#define regbh 6
#else
#define regal 0
#define regah 1
#define regcl 2
#define regch 3
#define regdl 4
#define regdh 5
#define regbl 6
#define regbh 7
#endif

#define CPU_BOOT_FD0 0x00
#define CPU_BOOT_FD1 0x01
#define CPU_BOOT_HD0 0x80
#define CPU_BOOT_HD1 0x81
#define CPU_BOOT_DEFAULT 0xFE
#define CPU_BOOT_ROM_BASIC 0xFF

union _bytewordregs_ {
    uint16_t wordregs[8];
    uint8_t byteregs[8];
};

uint16_t getreg16(uint8_t regid);

uint8_t cpu_get_bootdrive(void);

void cpu_set_bootdrive(uint8_t value);

uint8_t cpu_did_bootstrap(void);

uint8_t cpu_get_running(void);

void cpu_set_running(uint8_t value);

