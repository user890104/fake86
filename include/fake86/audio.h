/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2012 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

void audio_init(void);

void audio_deinit(void);

uint8_t audio_get_enabled(void);

void audio_set_enabled(uint8_t value);

uint8_t audio_is_buffer_filled(void);

void audio_tick(void);

void audio_set_speaker(uint8_t value);

uint8_t audio_get_sndsource(void);

void audio_set_sndsource(uint8_t value);

uint32_t audio_get_sample_rate(void);

void audio_set_sample_rate(uint32_t value);

uint32_t audio_get_latency(void);

void audio_set_latency(uint32_t value);
