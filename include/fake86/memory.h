/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

void memory_write8(uint32_t address, uint8_t value);

void memory_write16(uint32_t address, uint16_t value);

uint8_t memory_read8(uint32_t address);

uint16_t memory_read16(uint32_t address);

void memory_write8_direct(uint32_t address, uint8_t value);

void memory_write16_direct(uint32_t address, uint16_t value);

uint8_t memory_read8_direct(uint32_t address);

uint16_t memory_read16_direct(uint32_t address);

void *memory_read(void *buf, uint32_t offset, size_t size);

void *memory_write(void *buf, uint32_t offset, size_t size);

uint32_t memory_load_binary(uint32_t address, const char *filename, const uint8_t roflag);

uint32_t memory_load_bios(void);

uint32_t memory_load_rom(uint32_t address, const char *filename, const uint8_t failure_fatal);

void memory_set_bios_file(char *value);

void memory_clear_video(void);

void *memory_receive_packet(const void *buf, const size_t len);

void *memory_copy_packet(const size_t len);

void *memory_send_packet(void *buf, const size_t len);
