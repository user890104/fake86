/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2012 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

void ports_init(void);

void port_out(uint16_t portnum, uint8_t value);

uint8_t port_in(uint16_t portnum);

void port_out_16(uint16_t portnum, uint16_t value);

uint16_t port_in_16(uint16_t portnum);

void ports_set_write_redirector(uint16_t startport, uint16_t endport, void (*callback)(uint16_t, uint8_t));

void ports_set_read_redirector(uint16_t startport, uint16_t endport, uint8_t (*callback)(uint16_t));

void ports_set_write_redirector_16(uint16_t startport, uint16_t endport, void (*callback)(uint16_t, uint16_t));

void ports_set_read_redirector_16(uint16_t startport, uint16_t endport, uint16_t (*callback)(uint16_t));
