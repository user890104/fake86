/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2012 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

void render_set_constantw(uint16_t value);

void render_set_constanth(uint16_t value);

void render_set_noscale(uint8_t value);

void render_set_smooth(uint8_t value);

uint8_t render_get_benchmark(void);

void render_set_benchmark(uint8_t value);

uint8_t render_get_fullscreen(void);

void render_set_fullscreen(uint8_t value);

void render_set_framedelay(uint32_t value);
