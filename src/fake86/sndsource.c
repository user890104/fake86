/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* ssource.c: functions to emulate the Disney Sound Source's 16-byte FIFO buffer. */

#include "fake86/config.h"
#include <stdint.h>
#include "fake86/sndsource.h"
#include "fake86/ports.h"

static void putssourcebyte(uint8_t value);
static uint8_t ssourcefull(void);
static void outsoundsource(uint16_t portnum, uint8_t value);
static uint8_t insoundsource(uint16_t portnum);

static uint8_t ssourcebuf[16], ssourceptr = 0, ssourceactive = 0;
static int16_t ssourcecursample = 0;
static uint8_t last37a = 0;

void sndsource_init(void) {
    ports_set_write_redirector(0x378, 0x378, &outsoundsource);
    ports_set_read_redirector(0x379, 0x379, &insoundsource);
    ports_set_write_redirector(0x37A, 0x37A, &outsoundsource);
    ssourceactive = 1;
}

int16_t sndsource_get_byte(void) {
    return ssourcecursample;
}

void sndsource_tick(void) {
    uint8_t rotatefifo;
    if ((ssourceptr == 0) || (!ssourceactive)) {
        ssourcecursample = 0;
        return;
    }
    ssourcecursample = ssourcebuf[0];
    for (rotatefifo = 1; rotatefifo < 16; rotatefifo++) {
        ssourcebuf[rotatefifo - 1] = ssourcebuf[rotatefifo];
    }
    ssourceptr--;
    port_out(0x379, 0);
}

static void putssourcebyte(uint8_t value) {
    if (ssourceptr == 16) {
        return;
    }
    ssourcebuf[ssourceptr++] = value;
    if (ssourceptr == 16) {
        port_out(0x379, 0x40);
    }
}

static uint8_t ssourcefull(void) {
    return ssourceptr == 16 ? 0x40 : 0x00;
}

static void outsoundsource(uint16_t portnum, uint8_t value) {
    switch (portnum) {
        case 0x378:
            putssourcebyte(value);
            break;
        case 0x37A:
            if (value & 4 && !(last37a & 4)) {
                putssourcebyte(port_in(0x378));
            }
            last37a = value;
            break;
    }
}

static uint8_t insoundsource(uint16_t portnum) {
    (void) portnum;
    return ssourcefull();
}
