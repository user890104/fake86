/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* ports.c: functions to handle port I/O from the CPU module, as well
   as functions for emulated hardware components to register their
   read/write callback functions across the port address range. */

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "fake86/audio.h"
#include "fake86/cpu.h"
#include "fake86/main.h"
#include "fake86/ports.h"

uint8_t portram[0x10000]; // TODO

static void (*port_write_callback[0x10000])(uint16_t, uint8_t);

static uint8_t (*port_read_callback[0x10000])(uint16_t);

static void (*port_write_callback16[0x10000])(uint16_t, uint16_t);

static uint16_t (*port_read_callback16[0x10000])(uint16_t);

void ports_init(void) {
    memset(port_write_callback, 0, sizeof(port_write_callback));
    memset(port_read_callback, 0, sizeof(port_read_callback));
    memset(port_write_callback16, 0, sizeof(port_write_callback16));
    memset(port_read_callback16, 0, sizeof(port_read_callback16));
}

void port_out(uint16_t portnum, uint8_t value) {
    portram[portnum] = value;
    if (main_get_verbose()) {
        printf("port_out(0x%X, 0x%02X);\n", portnum, value);
    }
    switch (portnum) {
        case 0x61:
            audio_set_speaker((value & 3) == 3);
            return;
    }
    if (port_write_callback[portnum] != NULL) {
        (*port_write_callback[portnum])(portnum, value);
    }
}

uint8_t port_in(uint16_t portnum) {
    if (main_get_verbose()) {
        printf("port_in(0x%X);\n", portnum);
    }
    switch (portnum) {
        case 0x62:
            return 0x00;
        case 0x60:
        case 0x61:
        case 0x63:
        case 0x64:
        case 0x378:
            return portram[portnum];
    }
    if (port_read_callback[portnum] != NULL) {
        return (*port_read_callback[portnum])(portnum);
    }
    return 0xFF;
}

void port_out_16(uint16_t portnum, uint16_t value) {
    if (port_write_callback16[portnum] != NULL) {
        (*port_write_callback16[portnum])(portnum, value);
        return;
    }

    port_out(portnum, (uint8_t) value);
    port_out(portnum + 1, (uint8_t)(value >> 8));
}

uint16_t port_in_16(uint16_t portnum) {
    if (port_read_callback16[portnum] != NULL) {
        return (*port_read_callback16[portnum])(portnum);
    }

    uint16_t ret;
    ret = (uint16_t) port_in(portnum);
    ret |= (uint16_t) port_in(portnum + 1) << 8;
    return ret;
}

void ports_set_write_redirector(uint16_t startport, uint16_t endport, void (*callback)(uint16_t, uint8_t)) {
    uint16_t i;
    for (i = startport; i <= endport; i++) {
        port_write_callback[i] = callback;
    }
}

void ports_set_read_redirector(uint16_t startport, uint16_t endport, uint8_t (*callback)(uint16_t)) {
    uint16_t i;
    for (i = startport; i <= endport; i++) {
        port_read_callback[i] = callback;
    }
}

void ports_set_write_redirector_16(uint16_t startport, uint16_t endport, void (*callback)(uint16_t, uint16_t)) {
    uint16_t i;
    for (i = startport; i <= endport; i++) {
        port_write_callback16[i] = callback;
    }
}

void ports_set_read_redirector_16(uint16_t startport, uint16_t endport, uint16_t (*callback)(uint16_t)) {
    uint16_t i;
    for (i = startport; i <= endport; i++) {
        port_read_callback16[i] = callback;
    }
}
