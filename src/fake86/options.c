/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* parsecl.c: Fake86 command line parsing for runtime options. */

#include "fake86/config.h"
#include "SDL.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "fake86/audio.h"
#include "fake86/cpu.h"
#include "fake86/disk.h"
#include "fake86/main.h"
#include "fake86/memory.h"
#include "fake86/packet.h"
#include "fake86/render.h"
#include "fake86/timing.h"

extern uint8_t insertdisk(uint8_t drivenum, char *filename);

static void showhelp(void);

static uint32_t hextouint(char *src);

void options_parse(int argc, char *argv[]) {
    int i;

    if (argc < 2) {
        printf("Invoke Fake86 with the parameter -h for help and usage information.\n");
    }

    for (i = 1; i < argc; i++) {
        if (strcasecmp(argv[i], "-h") == 0 || strcasecmp(argv[i], "-?") == 0 || strcasecmp(argv[i], "-help") == 0) {
            showhelp();
            exit(0);
        }

        if (strcasecmp(argv[i], "-fd0") == 0) {
            if (insertdisk(0, argv[++i])) {
                printf("ERROR: Unable to open image file %s\n", argv[i]);
            }
            continue;
        }

        if (strcasecmp(argv[i], "-fd1") == 0) {
            if (insertdisk(1, argv[++i])) {
                printf("ERROR: Unable to open image file %s\n", argv[i]);
            }
            continue;
        }

        if (strcasecmp(argv[i], "-hd0") == 0) {
            if (insertdisk(0x80, argv[++i])) {
                printf("ERROR: Unable to open image file %s\n", argv[i]);
            }
            continue;
        }

        if (strcasecmp(argv[i], "-hd1") == 0) {
            if (insertdisk(0x81, argv[++i])) {
                printf("ERROR: Unable to open image file %s\n", argv[i]);
            }
            continue;
        }

        if (strcasecmp(argv[i], "-boot") == 0) {
            if (strcasecmp(argv[++i], "rom") == 0) {
                cpu_set_bootdrive(CPU_BOOT_ROM_BASIC);
            }
            else {
                cpu_set_bootdrive(atoi(argv[i]));
            }
            continue;
        }

        if (strcasecmp (argv[i], "-bios") == 0) {
            memory_set_bios_file(argv[++i]);
            continue;
        }

        if (strcasecmp(argv[i], "-net") == 0) {
            if (strcasecmp(argv[++i], "list") == 0) {
                packet_set_ethif(PACKET_ETHIF_LIST);
            }
            else {
                packet_set_ethif(atoi(argv[i]));
            }
            continue;
        }

        if (strcasecmp(argv[i], "-nosound") == 0) {
            audio_set_enabled(0);
            continue;
        }

        if (strcasecmp(argv[i], "-fullscreen") == 0) {
            render_set_fullscreen(1);
            continue;
        }

        if (strcasecmp(argv[i], "-verbose") == 0) {
            main_set_verbose(1);
            continue;
        }

        if (strcasecmp(argv[i], "-delay") == 0) {
            render_set_framedelay(atol(argv[++i]));
            continue;
        }

        if (strcasecmp(argv[i], "-slowsys") == 0) {
            timing_set_slow_system(1);
            continue;
        }

        if (strcasecmp(argv[i], "-resw") == 0) {
            render_set_constantw((uint16_t) atoi(argv[++i]));
            continue;
        }

        if (strcasecmp(argv[i], "-resh") == 0) {
            render_set_constanth((uint16_t) atoi(argv[++i]));
            continue;
        }

        if (strcasecmp(argv[i], "-smooth") == 0) {
            render_set_smooth(1);
            continue;
        }

        if (strcasecmp(argv[i], "-noscale") == 0) {
            render_set_noscale(1);
            continue;
        }

        if (strcasecmp(argv[i], "-ssource") == 0) {
            audio_set_sndsource(1);
            continue;
        }

        if (strcasecmp(argv[i], "-latency") == 0) {
            audio_set_latency(atol(argv[++i]));
            continue;
        }

        if (strcasecmp(argv[i], "-samprate") == 0) {
            audio_set_sample_rate(atol(argv[++i]));
            continue;
        }

        if (strcasecmp(argv[i], "-console") == 0) {
            main_set_use_console(1);
            continue;
        }

        if (strcasecmp(argv[i], "-oprom") == 0) {
            uint32_t option_rom_addr = hextouint(argv[++i]);
            memory_load_rom(option_rom_addr, argv[++i], 0);
            continue;
        }

        // undocumented
        if (strcasecmp(argv[i], "-speed") == 0) {
            main_set_speed((uint32_t) atol(argv[++i]));
            continue;
        }

        // undocumented
        if (strcasecmp(argv[i], "-fps") == 0) {
            render_set_benchmark(1);
            continue;
        }

        printf("Unrecognized parameter: %s\n", argv[i]);
        exit(1);
    }
}

static uint32_t hextouint(char *src) {
    uint32_t result = 0, cc;
    uint16_t i;

    for (i = 0; i < strlen(src); i++) {
        cc = src[i];

        if (cc == 0) {
            break;
        }

        if (cc >= 'a' && cc <= 'f') {
            cc -= 'a';
            cc += 10;
        }
        else {
            if (cc >= 'A' && cc <= 'F') {
                cc -= 'A';
                cc += 10;
            }
            else {
                if (cc >= '0' && cc <= '9') {
                    cc -= '0';
                }
                else {
                    return 0;
                }
            }
        }
        result <<= 4;
        result |= cc;
    }
    
    return result;
}

static void showhelp(void) {
    printf("Fake86 requires some command line parameters to run.\nValid options:\n");

    printf("  -fd0 filename    Specify a floppy disk image file to use as floppy 0.\n");
    printf("  -fd1 filename    Specify a floppy disk image file to use as floppy 1.\n");
    printf("  -hd0 filename    Specify a hard disk image file to use as hard drive 0.\n");
    printf("  -hd1 filename    Specify a hard disk image file to use as hard drive 1.\n");
    printf("  -boot #          Specify which BIOS drive ID should be the boot device in #.\n");
    printf("                   Examples: -boot 0 will boot from floppy 0.\n");
    printf("                             -boot 1 will boot from floppy 1.\n");
    printf("                             -boot 128 will boot from hard drive 0.\n");
    printf("                             -boot rom will boot to ROM BASIC if available.\n");
    printf("                   Default boot device is hard drive 0, if it exists.\n");
    printf("                   Otherwise, the default is floppy 0.\n");
    printf("  -bios filename   Specify alternate BIOS ROM image to use.\n");
#ifdef NETWORKING_ENABLED
#ifdef _WIN32
    printf("  -net #           Enable ethernet emulation via winpcap, where # is the\n");
#else
    printf("  -net #           Enable ethernet emulation via libpcap, where # is the\n");
#endif
    printf("                   numeric ID of your host's network interface to bridge.\n");
    printf("                   To get a list of possible interfaces, use -net list\n");
#endif
    printf("  -nosound         Disable audio emulation and output.\n");
    printf("  -fullscreen      Start Fake86 in fullscreen mode.\n");
    printf("  -verbose         Verbose mode. Operation details will be written to stdout.\n");
    printf("  -delay           Specify how many milliseconds the render thread should idle\n");
    printf("                   between drawing each frame. Default is 20 ms. (~50 FPS)\n");
    printf("  -slowsys         If your machine is very slow and you have audio dropouts,\n");
    printf("                   use this option to sacrifice audio quality to compensate.\n");
    printf("                   If you still have dropouts, then also decrease sample rate\n");
    printf("                   and/or increase latency.\n");
    printf("  -resw # -resh #  Force a constant window size in pixels.\n");
    printf("  -smooth          Apply smoothing to screen rendering.\n");
    printf("  -noscale         Disable 2x scaling of low resolution video modes.\n");
    printf("  -ssource         Enable Disney Sound Source emulation on LPT1.\n");
    printf("  -latency #       Change audio buffering and output latency. (default: 100 ms)\n");
    printf("  -samprate #      Change audio emulation sample rate. (default: 48000 Hz)\n");
    printf("  -console         Enable console on stdio during emulation.\n");
    printf("  -oprom addr rom  Inject a custom option ROM binary at an address in hex.\n");
    printf("                   Example: -oprom F4000 monitor.bin\n");
    printf("                            This loads the data from monitor.bin at 0xF4000.\n");

    printf("\nThis program is free software; you can redistribute it and/or\n");
    printf("modify it under the terms of the GNU General Public License\n");
    printf("as published by the Free Software Foundation; either version 2\n");
    printf("of the License, or (at your option) any later version.\n\n");

    printf("This program is distributed in the hope that it will be useful,\n");
    printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
    printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
    printf("GNU General Public License for more details.\n");
}
