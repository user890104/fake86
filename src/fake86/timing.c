/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* timing.c: critical functions to provide accurate timing for the
   system timer interrupt, and to generate new audio output samples. */

#include "fake86/config.h"
#include "SDL.h"
#include <stdint.h>
#include <stdio.h>

#include "fake86/adlib.h"
#include "fake86/audio.h"
#include "fake86/blaster.h"
#include "fake86/i8253.h"
#include "fake86/i8259.h"
#include "fake86/platform.h"
#include "fake86/sndsource.h"
#include "fake86/timing.h"

static uint64_t lasttick, tickgap, i8253tickgap, lasti8253tick, scanlinetiming, lastscanlinetick, curscanline;
static uint64_t sampleticks, lastsampletick, ssourceticks, lastssourcetick, adlibticks, lastadlibtick, lastblastertick;

static uint16_t pit0counter = 65535; // unused

extern uint8_t port3da;
static uint8_t slowsystem;

void timing_init(void) {
    uint64_t hostfreq = platform_get_host_freq();
    scanlinetiming = hostfreq / 31500;
    ssourceticks = hostfreq / 8000;
    adlibticks = hostfreq / 48000;
    if (audio_get_enabled()) {
        sampleticks = hostfreq / audio_get_sample_rate();
    }
    else {
        sampleticks = -1;
    }
    i8253tickgap = hostfreq / 119318;

    lasti8253tick = lastblastertick = lastadlibtick = lastssourcetick = lastsampletick = lastscanlinetick = lasttick = platform_get_tick();
}

void timing_tick(void) {
    uint8_t i8253chan;

    uint64_t curtick = platform_get_tick();

    if (curtick >= (lastscanlinetick + scanlinetiming)) {
        curscanline = (curscanline + 1) % 525;
        if (curscanline > 479) {
            port3da = 8;
        }
        else {
            port3da = 0;
        }
        if (curscanline & 1) {
            port3da |= 1;
        }
        pit0counter++;
        lastscanlinetick = curtick;
    }

    if (i8253_is_active(0)) { //timer interrupt channel on i8253
        if (curtick >= (lasttick + tickgap)) {
            lasttick = curtick;
            i8259_do_irq(0);
        }
    }

    if (curtick >= (lasti8253tick + i8253tickgap)) {
        for (i8253chan = 0; i8253chan < 3; i8253chan++) {
            if (i8253_is_active(i8253chan)) {
                if (i8253_get_counter(i8253chan) < 10) {
                    i8253_set_counter(i8253chan, i8253_get_data(i8253chan));
                }
                i8253_set_counter(i8253chan, i8253_get_counter(i8253chan) - 10);
            }
        }
        lasti8253tick = curtick;
    }

    if (curtick >= (lastssourcetick + ssourceticks)) {
        sndsource_tick();
        lastssourcetick = curtick - (curtick - (lastssourcetick + ssourceticks));
    }

    if (blaster_get_sample_rate() > 0) {
        if (curtick >= (lastblastertick + blaster_get_sample_ticks())) {
            blaster_tick();
            lastblastertick = curtick - (curtick - (lastblastertick + blaster_get_sample_ticks()));
        }
    }

    if (curtick >= (lastsampletick + sampleticks)) {
        audio_tick();
        if (slowsystem) {
            audio_tick();
            audio_tick();
            audio_tick();
        }
        lastsampletick = curtick - (curtick - (lastsampletick + sampleticks));
    }

    if (curtick >= (lastadlibtick + adlibticks)) {
        adlib_tick();
        lastadlibtick = curtick - (curtick - (lastadlibtick + adlibticks));
    }
}

void timing_set_tickgap(uint64_t value) {
    tickgap = value;
}

void timing_set_slow_system(uint8_t value) {
    slowsystem = value;
}
