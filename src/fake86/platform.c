/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* platform.c: platform-specific functions that need to be extended when porting
   to a new platform. basically everything platform-specific that is not provided
   by SDL goes here */

#include "fake86/platform.h"
#include <stdlib.h>

#ifdef _WIN32
#include <Windows.h>
#else

#include <sys/time.h>

#endif

#ifndef _WIN32
#ifndef __APPLE__
#include <X11/Xlib.h>
#include <unistd.h>
#endif
#endif

#ifdef _WIN32
static LARGE_INTEGER queryperf;
#else
static struct timeval tv;
#endif

static uint64_t hostfreq;

void platform_init(void) {
#ifndef _WIN32
#ifndef __APPLE__
    XInitThreads();
#endif
#endif

#ifdef _WIN32
    QueryPerformanceFrequency(&queryperf);
    hostfreq = queryperf.QuadPart;
#else
    hostfreq = 1e6;
#endif
}

uint64_t platform_get_host_freq(void) {
    return hostfreq;
}

uint64_t platform_get_tick(void) {
#ifdef _WIN32
    QueryPerformanceCounter(&queryperf);
    return queryperf.QuadPart;
#else
    gettimeofday(&tv, NULL);
    return (uint64_t) tv.tv_sec * (uint64_t) 1e6 + (uint64_t) tv.tv_usec;
#endif
}
