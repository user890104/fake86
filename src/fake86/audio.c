/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* audio.c: functions to mix the audio channels, and handle SDL's audio interface. */

#include "fake86/config.h"
#include "SDL.h"
#include <stdint.h>
#include <stdio.h>
#include <memory.h>
#include "fake86/adlib.h"
#include "fake86/audio.h"
#include "fake86/blaster.h"
#include "fake86/sndsource.h"
#include "fake86/speaker.h"

#ifdef AUDIO_RECORD_WAV
struct wav_hdr_s {
    uint8_t	RIFF[4];	/* RIFF Header	  */ //Magic header
    uint32_t ChunkSize;	  /* RIFF Chunk Size  */
    uint8_t	WAVE[4];	/* WAVE Header	  */
    uint8_t	fmt[4];	 /* FMT header	   */
    uint32_t Subchunk1Size;  /* Size of the fmt chunk				*/
    uint16_t AudioFormat;	/* Audio format 1=PCM,6=mulaw,7=alaw, 257=IBM Mu-Law, 258=IBM A-Law, 259=ADPCM */
    uint16_t NumOfChan;	  /* Number of channels 1=Mono 2=Sterio		   */
    uint32_t SamplesPerSec;  /* Sampling Frequency in Hz				 */
    uint32_t bytesPerSec;	/* bytes per second */
    uint16_t blockAlign;	 /* 2=16-bit mono, 4=16-bit stereo */
    uint16_t bitsPerSample;  /* Number of bits per sample	  */
    uint8_t	Subchunk2ID[4]; /* "data"  string   */
    uint32_t Subchunk2Size;  /* Sampled data length	*/
};

static struct wav_hdr_t wav_hdr;
static FILE *wav_file = NULL;
#endif

static SDL_AudioSpec wanted;
static uint8_t audbuf[96000], audioenabled = 1, speakerenabled, sndsource_enabled;
static uint32_t audbufptr, buffer_size, sample_rate = AUDIO_DEFAULT_SAMPLE_RATE, latency = AUDIO_DEFAULT_LATENCY;

static void fill_audio(void *udata, uint8_t *stream, int len);

void audio_init(void) {
    printf("Initializing audio stream... ");

    if (sample_rate < 4000) {
        sample_rate = 4000;
    }
    if (sample_rate > 96000) {
        sample_rate = 96000;
    }
    if (latency < 10) {
        latency = 10;
    }
    if (latency > 1000) {
        latency = 1000;
    }
    audbufptr = buffer_size = (sample_rate / 1000) * latency;

    wanted.freq = sample_rate;
    wanted.format = AUDIO_U8;
    wanted.channels = 1;
    wanted.samples = (uint16_t) buffer_size >> 1;
    wanted.callback = fill_audio;
    wanted.userdata = NULL;

    if (SDL_OpenAudio(&wanted, NULL) < 0) {
        printf("Error: %s\n", SDL_GetError());
        return;
    }
    else {
        printf("OK! (%u Hz, %u ms, %u sample latency)\n",
               sample_rate, latency, buffer_size);
    }

    memset(audbuf, 128, sizeof(audbuf));
    audbufptr = buffer_size;
#ifdef AUDIO_RECORD_WAV
    create_output_wav("fake86.wav");
#endif
    SDL_PauseAudio(0);
}

void audio_deinit(void) {
    SDL_PauseAudio(1);
#ifdef AUDIO_RECORD_WAV
    if (wav_file == NULL) {
        return;
    }
    wav_hdr.ChunkSize = wav_hdr.Subchunk2Size + sizeof(wav_hdr) - 8;
    fseek(wav_file, 0, SEEK_SET);
    fwrite((void *) &wav_hdr, 1, sizeof(wav_hdr), wav_file);
    fclose(wav_file);
#endif
}

uint8_t audio_get_enabled(void) {
    return audioenabled;
}

void audio_set_enabled(uint8_t value) {
    audioenabled = value;
}

uint32_t audio_get_sample_rate(void) {
    return sample_rate;
}

void audio_set_sample_rate(uint32_t value) {
    sample_rate = value;
}

uint32_t audio_get_latency(void) {
    return latency;
}

void audio_set_latency(uint32_t value) {
    latency = value;
}

uint8_t audio_is_buffer_filled(void) {
    return audbufptr >= buffer_size;
}

void audio_tick(void) {
    if (audbufptr >= buffer_size) {
        return;
    }
    int16_t sample = adlib_gen_sample() >> 4;
    if (sndsource_enabled) {
        sample += sndsource_get_byte();
    }
    sample += blaster_get_sample();
    if (speakerenabled) {
        sample += speaker_get_sample() >> 1;
    }
    if (audbufptr < sizeof(audbuf)) {
        audbuf[audbufptr++] = (uint8_t)((uint16_t) sample + 128);
    }
}

void audio_set_speaker(uint8_t value) {
    speakerenabled = value;
}

uint8_t audio_get_sndsource(void) {
    return sndsource_enabled;
}

void audio_set_sndsource(uint8_t value) {
    sndsource_enabled = value;
}

static void fill_audio(void *udata, uint8_t *stream, int len) {
    (void) udata;
    memcpy(stream, audbuf, len);
    memmove(audbuf, &audbuf[len], buffer_size - len);

    if ((unsigned int) len <= audbufptr) {
        audbufptr -= len;
    }
    else {
        audbufptr = 0;
    }
}

#ifdef AUDIO_RECORD_WAV
static void create_output_wav(char *filename) {
    printf("Creating %s for audio logging... ", filename);
    wav_file = fopen(filename, "wb");
    if (wav_file == NULL) {
        printf("failed!\n");
        return;
    }
    printf("OK!\n");

    wav_hdr.AudioFormat = 1; //PCM
    wav_hdr.bitsPerSample = 8;
    wav_hdr.blockAlign = 1;
    wav_hdr.ChunkSize = sizeof(wav_hdr) - 4;
    memcpy(&wav_hdr.WAVE[0], "WAVE", 4);
    memcpy(&wav_hdr.fmt[0], "fmt ", 4);
    wav_hdr.NumOfChan = 1;
    wav_hdr.bytesPerSec = sample_rate * (uint32_t)(wav_hdr.bitsPerSample >> 3) * (uint32_t) wav_hdr.NumOfChan;
    memcpy(&wav_hdr.RIFF[0], "RIFF", 4);
    wav_hdr.Subchunk1Size = 16;
    wav_hdr.SamplesPerSec = sample_rate;
    memcpy(&wav_hdr.Subchunk2ID[0], "data", 4);
    wav_hdr.Subchunk2Size = 0;
    fwrite((void *)&wav_hdr, 1, sizeof(wav_hdr), wav_file);
}
#endif
