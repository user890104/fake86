/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* memory.c: access to RAM/ROM */

#include <fake86/config.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <fake86/cpu.h>
#include <fake86/memory.h>

static char *biosfile = PATH_DATAFILES
"pcxtbios.bin";

static uint8_t memory[MEMORY_SIZE], readonly[MEMORY_SIZE];

extern uint8_t hdcount;

extern void writeVGA(uint32_t addr32, uint8_t value);

extern uint8_t readVGA(uint32_t addr32);

extern uint16_t VGA_SC[0x100], VGA_CRTC[0x100], VGA_ATTR[0x100], VGA_GC[0x100];
extern uint8_t vidmode;
extern uint8_t updatedscreen;

extern union _bytewordregs_ regs;
extern uint16_t	segregs[4];

void memory_write8(uint32_t address, uint8_t value) {
    address &= MEMORY_SIZE - 1;

#ifdef CPU_ADDR_MODE_CACHE
    if (!readonly[address]) {
        addrcachevalid[address] = 0;
    }
#endif
    if (readonly[address] || address >= 0xC0000) {
        return;
    }

    if (address >= 0xA0000 && address <= 0xBFFFF) {
        if (vidmode != 0x13 && vidmode != 0x12 && vidmode != 0xD && vidmode != 0x10) {
            memory[address] = value;
        }
        else {
            if ((VGA_SC[4] & 6) == 0 && vidmode != 0xD && vidmode != 0x10 && vidmode != 0x12) {
                memory[address] = value;
            }
            else {
                writeVGA(address - 0xA0000, value);
            }
        }

        updatedscreen = 1;
    }
    else {
        memory[address] = value;
    }
}

void memory_write16(uint32_t address, uint16_t value) {
    memory_write8(address, (uint8_t) value);
    memory_write8(address + 1, (uint8_t)(value >> 8));
}

uint8_t memory_read8(uint32_t address) {
    address &= MEMORY_SIZE - 1;

    if (address >= 0xA0000 && address <= 0xBFFFF) {
        if (vidmode == 0xD || vidmode == 0xE || vidmode == 0x10 || vidmode == 0x12) {
            return readVGA(address - 0xA0000);
        }
        if (vidmode != 0x13 && vidmode != 0x12 && vidmode != 0xD) {
            return memory[address];
        }
        if ((VGA_SC[4] & 6) == 0) {
            return memory[address];
        }
        else {
            return readVGA(address - 0xA0000);
        }
    }

    if (!cpu_did_bootstrap()) {
        memory[0x410] = 0x41; //ugly hack to make BIOS always believe we have an EGA/VGA card installed
        memory[0x475] = hdcount; //the BIOS doesn't have any concept of hard drives, so here's another hack
    }

    return memory[address];
}

uint16_t memory_read16(uint32_t address) {
    return ((uint16_t) memory_read8(address) | (uint16_t)(memory_read8(address + 1) << 8));
}

// TODO: merge into above functions
void memory_write8_direct(uint32_t address, uint8_t value) {
    address &= MEMORY_SIZE - 1;
    memory[address] = value;
}

void memory_write16_direct(uint32_t address, uint16_t value) {
    memory_write8_direct(address, (uint8_t) value);
    memory_write8_direct(address + 1, (uint8_t)(value >> 8));
}

uint8_t memory_read8_direct(uint32_t address) {
    address &= MEMORY_SIZE - 1;
    return memory[address];
}

uint16_t memory_read16_direct(uint32_t address) {
    return ((uint16_t) memory_read8_direct(address) | (uint16_t)(memory_read8_direct(address + 1) << 8));
}

void *memory_read(void *buf, uint32_t offset, size_t size) {
    return memcpy(buf, &memory[offset], size);
}

void *memory_write(void *buf, uint32_t offset, size_t size) {
    return memcpy(&memory[offset], buf, size);
}

uint32_t memory_load_binary(uint32_t address, const char *filename, const uint8_t roflag) {
    FILE *binfile = NULL;
    uint32_t binsize;
    uint32_t loadedsize;

    binfile = fopen(filename, "rb");

    if (binfile == NULL) {
        return 0;
    }

    fseek(binfile, 0, SEEK_END);
    binsize = ftell(binfile);
    fseek(binfile, 0, SEEK_SET);

    if (address == MEMORY_SIZE) {
        // special case for BIOS
        address = MEMORY_SIZE - binsize;
    }

    loadedsize = fread(&memory[address], 1, binsize, binfile);
    fclose(binfile);

    if (loadedsize != binsize) {
        return 0;
    }

    memset(&readonly[address], roflag, binsize);

    return binsize;
}

uint32_t memory_load_bios(void) {
    if (!biosfile) {
        return 0;
    }

    return memory_load_binary(MEMORY_SIZE, biosfile, 1);
}

uint32_t memory_load_rom(uint32_t address, const char *filename, const uint8_t failure_fatal) {
    uint32_t readsize;
    readsize = memory_load_binary(address, filename, 1);
    if (!readsize) {
        if (failure_fatal) {
            printf("FATAL: ");
        }
        else {
            printf("WARNING: ");
        }
        printf("Unable to load %s\n", filename);
        return 0;
    }
    printf("Loaded %s at 0x%05X (%u KB)\n", filename, address, readsize >> 10);
    return readsize;
}

void memory_set_bios_file(char *value) {
    biosfile = value;
}

void memory_clear_video(void) {
    memset(&memory[0xA0000], 0, 0x1FFFF);
}

void *memory_receive_packet(const void *buf, const size_t len) {
    return memcpy(&memory[0xD0000], buf, len);
}

void *memory_copy_packet(const size_t len) {
    return memcpy(&memory[((uint32_t) segregs[reges] << 4) + (uint32_t) regs.wordregs[regdi]], &memory[0xD0000], len);
}

void *memory_send_packet(void *buf, const size_t len) {
    return memcpy(buf, &memory[((uint32_t) segregs[regds] << 4) + (uint32_t) regs.wordregs[regsi]], len);
}
