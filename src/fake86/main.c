/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* main.c: functions to initialize the different components of Fake86,
   load ROM binaries, and kickstart the CPU emulator. */

#include "fake86/config.h"
#include "SDL.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "fake86/adlib.h"
#include "fake86/audio.h"
#include "fake86/blaster.h"
#include "fake86/cpu.h"
#include "fake86/disk.h"
#include "fake86/i8237.h"
#include "fake86/i8253.h"
#include "fake86/i8259.h"
#include "fake86/input.h"
#include "fake86/main.h"
#include "fake86/memory.h"
#include "fake86/options.h"
#include "fake86/packet.h"
#include "fake86/platform.h"
#include "fake86/ports.h"
#include "fake86/render.h"
#include "fake86/sermouse.h"
#include "fake86/sndsource.h"
#include "fake86/timing.h"

SDL_Thread *console_thread;
SDL_Thread *emulator_thread;
extern SDL_Thread *video_thread;
extern SDL_mutex *screen_mutex;

const char *build = BUILD_STRING;

extern struct struct_drive disk[256];

extern void reset86();
extern void exec86 (uint32_t execloops);
extern uint8_t initscreen (const char *ver);
extern void VideoThread();
extern void doscrmodechange();

#ifdef CPU_ADDR_MODE_CACHE
extern uint64_t cached_access_count, uncached_access_count;
#endif

extern uint8_t scrmodechange;
extern uint64_t totalexec, totalframes;
uint64_t starttick, seconds;

uint8_t verbose;
static uint8_t useconsole;
static uint32_t speed;

extern uint32_t SDL_GetTicks();
extern uint8_t insertdisk (uint8_t drivenum, char *filename);
extern void ejectdisk (uint8_t drivenum);
extern uint8_t net_enabled;
//extern void isa_ne2000_init(int baseport, uint8_t irq);
extern void initVideoPorts();
extern void isa_ne2000_init (uint16_t baseport, uint8_t irq);

#ifdef NETWORKING_ENABLED
extern void initpcap();
extern void dispatch();
#endif

void main_set_speed(uint32_t value) {
	speed = value;
}

void main_set_use_console(uint8_t value) {
	useconsole = value;
}

uint8_t main_get_verbose(void) {
	return verbose;
}

void main_set_verbose(uint8_t value) {
	verbose = value;
}

void printbinary (uint8_t value) {
	int8_t curbit;

	for (curbit=7; curbit>=0; curbit--) {
			if ( (value >> curbit) & 1) printf ("1");
			else printf ("0");
		}
}

void inithardware() {
#ifdef NETWORKING_ENABLED
	if (packet_get_ethif() != PACKET_ETHIF_NONE) initpcap();
#endif
	printf ("Initializing emulated hardware:\n");
	ports_init();
	printf ("  - Intel 8253 timer: ");
	i8253_init();
	printf ("OK\n");
	printf ("  - Intel 8259 interrupt controller: ");
	i8259_init();
	printf ("OK\n");
	printf ("  - Intel 8237 DMA controller: ");
    i8237_init();
	printf ("OK\n");
	initVideoPorts();
	if (audio_get_sndsource()) {
		printf ("  - Disney Sound Source: ");
		sndsource_init();
		printf ("OK\n");
	}
#ifndef NETWORKING_OLDCARD
	printf ("  - Novell NE2000 ethernet adapter: ");
	isa_ne2000_init (0x300, 6);
	printf ("OK\n");
#endif
	printf ("  - Adlib FM music card: ");
	adlib_init(0x388);
	printf ("OK\n");
	printf ("  - Creative Labs Sound Blaster 2.0: ");
	blaster_init(0x220, 7);
	printf ("OK\n");
	printf ("  - Serial mouse (Microsoft compatible): ");
	sermouse_init(0x3F8, 4);
	printf ("OK\n");
	if (audio_get_enabled()) {
	    audio_init();
    }
	platform_init();
	timing_init();
	initscreen (build);
}

uint8_t dohardreset;

#ifdef _MSC_VER
void initmenus();
#endif

static int runemulator(void *dummy) {
	(void)dummy;
	while (cpu_get_running()) {
			if (!speed) exec86 (10000);
			else {
				exec86(speed / 100);
				while (!audio_is_buffer_filled()) {
					timing_tick();
					audio_tick();
				}

				SDL_Delay(10);
			}
			if (scrmodechange) doscrmodechange();
			if (dohardreset) {
				reset86();
				dohardreset = 0;
			}
		}
	return 0;
}

extern int runconsole(void *dummy);
int main (int argc, char *argv[]) {
	uint32_t biossize;

	printf ("%s (c)2010-2013 Mike Chambers\n", build);
	printf ("[A portable, open-source 8086 PC emulator]\n\n");

	options_parse(argc, argv);

	// Default boot device
    if (cpu_get_bootdrive() == CPU_BOOT_DEFAULT) {
        if (disk[0x80].inserted) {
            cpu_set_bootdrive(CPU_BOOT_HD0);
        }
        else {
            if (disk[0x00].inserted) {
                cpu_set_bootdrive(CPU_BOOT_FD0);
            }
            else {
                cpu_set_bootdrive(CPU_BOOT_ROM_BASIC);
            }
        }
    }

	biossize = memory_load_bios();
	if (!biossize) return (-1);
#ifdef DISK_CONTROLLER_ATA
	if (!memory_load_rom(0xD0000UL, PATH_DATAFILES "ide_xt.bin", 1)) {
	    return (-1);
    }
#endif
	if (biossize <= 8192) {
		memory_load_rom(0xF6000UL, PATH_DATAFILES "rombasic.bin", 0);
		if (!memory_load_rom(0xC0000UL, PATH_DATAFILES "videorom.bin", 1)) {
		    return (-1);
        }
	}
	printf ("\nInitializing CPU... ");
	cpu_set_running(1);
	reset86();
	printf ("OK!\n");

	inithardware();

#ifdef _MSC_VER
	initmenus();
#endif
	if (useconsole) {
		console_thread = SDL_CreateThread(runconsole, NULL);
		if (console_thread == NULL) {
			printf("SDL_CreateThread failed: %s\n", SDL_GetError());
			exit(1);
		}
	}

	emulator_thread = SDL_CreateThread(runemulator, NULL);
	
	if (emulator_thread == NULL) {
		printf("SDL_CreateThread failed: %s\n", SDL_GetError());
		exit(1);
	}

	starttick = SDL_GetTicks();
	while (cpu_get_running()) {
			input_handle();
#ifdef NETWORKING_ENABLED
			if (packet_get_ethif() < PACKET_ETHIF_NONE) dispatch();
#endif
			SDL_Delay(1);
	}
	
	SDL_WaitThread(emulator_thread, NULL);
	
	if (useconsole) {
		SDL_WaitThread(console_thread, NULL);
	}
	
	SDL_WaitThread(video_thread, NULL);
	
	SDL_DestroyMutex(screen_mutex);

	seconds = (SDL_GetTicks() - starttick) / 1000;
	if (seconds == 0) seconds = 1; //avoid divide-by-zero exception in the code below, if ran for less than 1 second

	audio_deinit();

	if (render_get_benchmark()) {
			printf ("\n%" PRIu64 " frames rendered in %" PRIu64 "u seconds.\n", totalframes, seconds);
			printf ("Average framerate: %" PRIu64 " FPS.\n", totalframes / seconds);
		}

	printf ("\n%" PRIu64 " instructions executed in %" PRIu64 " seconds.\n", totalexec, seconds);
	printf ("Average speed: %" PRIu64 " instructions/second.\n", totalexec / seconds);

#ifdef CPU_ADDR_MODE_CACHE
	printf ("\n  Cached modregrm data access count: %" PRIu64 "\n", cached_access_count);
	printf ("Uncached modregrm data access count: %" PRIu64 "\n", uncached_access_count);
#endif

	if (useconsole)	exit (0); //makes sure console thread quits even if blocking

	return (0);
}
