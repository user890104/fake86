/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* i8253.c: functions to emulate the Intel 8253 programmable interval timer.
   these are required for the timer interrupt and PC speaker to be
   properly emulated! */

#include <stdint.h>
#include <stdio.h>
#include <memory.h>
#include "fake86/i8253.h"
#include "fake86/platform.h"
#include "fake86/ports.h"
#include "fake86/timing.h"

#define PIT_MODE_LATCHCOUNT    0
#define PIT_MODE_LOBYTE    1
#define PIT_MODE_HIBYTE    2
#define PIT_MODE_TOGGLE    3

struct i8253_t {
    uint16_t chandata[3];
    uint8_t accessmode[3];
    uint8_t bytetoggle[3];
    uint32_t effectivedata[3];
    float chanfreq[3];
    uint8_t active[3];
    uint16_t counter[3];
};

static void out8253(uint16_t portnum, uint8_t value);

static uint8_t in8253(uint16_t portnum);

static struct i8253_t i8253;

void i8253_init() {
    memset(&i8253, 0, sizeof(i8253));
    ports_set_write_redirector(0x40, 0x43, &out8253);
    ports_set_read_redirector(0x40, 0x43, &in8253);
}

uint8_t i8253_is_active(uint8_t channel) {
    return i8253.active[channel];
}

uint16_t i8253_get_counter(uint8_t channel) {
    return i8253.counter[channel];
}

void i8253_set_counter(uint8_t channel, uint16_t value) {
    i8253.counter[channel] = value;
}

uint16_t i8253_get_data(uint8_t channel) {
    return i8253.chandata[channel];
}

float i8253_get_frequency(uint8_t channel) {
    return i8253.chanfreq[channel];
}

static void out8253(uint16_t portnum, uint8_t value) {
    uint8_t highbyte = 0;
    portnum &= 3;

    switch (portnum) {
        case 0:
        case 1:
        case 2: //channel data
            if (i8253.accessmode[portnum] == PIT_MODE_LOBYTE ||
                (i8253.accessmode[portnum] == PIT_MODE_TOGGLE && i8253.bytetoggle[portnum] == 0)) {
                highbyte = 0;
            }
            else {
                if (i8253.accessmode[portnum] == PIT_MODE_HIBYTE ||
                    (i8253.accessmode[portnum] == PIT_MODE_TOGGLE && i8253.bytetoggle[portnum] == 1)) {
                    highbyte = 1;
                }
            }
            if (highbyte) { //high byte
                i8253.chandata[portnum] = (i8253.chandata[portnum] & 0x00FF) | ((uint16_t) value << 8);
            }
            else { //low byte
                i8253.chandata[portnum] = (i8253.chandata[portnum] & 0xFF00) | value;
            }
            if (i8253.chandata[portnum] == 0) {
                i8253.effectivedata[portnum] = 65536;
            }
            else {
                i8253.effectivedata[portnum] = i8253.chandata[portnum];
            }
            i8253.active[portnum] = 1;
            timing_set_tickgap((uint64_t)((float) platform_get_host_freq() / (float) ((float) 1193182 / (float) i8253.effectivedata[0])));
            if (i8253.accessmode[portnum] == PIT_MODE_TOGGLE) {
                i8253.bytetoggle[portnum] = (~i8253.bytetoggle[portnum]) & 1;
            }
            i8253.chanfreq[portnum] =
                    (float) ((uint32_t)(((float) 1193182.0 / (float) i8253.effectivedata[portnum]) * (float) 1000.0)) /
                    (float) 1000.0;
            //printf("[DEBUG] PIT channel %u counter changed to %u (%f Hz)\n", portnum, i8253.chandata[portnum], i8253.chanfreq[portnum]);
            break;
        case 3: //mode/command
            i8253.accessmode[value >> 6] = (value >> 4) & 3;
            if (i8253.accessmode[value >> 6] == PIT_MODE_TOGGLE) {
                i8253.bytetoggle[value >> 6] = 0;
            }
            break;
    }
}

static uint8_t in8253(uint16_t portnum) {
    uint8_t highbyte = 0;
    portnum &= 3;

    switch (portnum) {
        case 0:
        case 1:
        case 2: //channel data
            if (i8253.accessmode[portnum] == 0 || i8253.accessmode[portnum] == PIT_MODE_LOBYTE ||
                (i8253.accessmode[portnum] == PIT_MODE_TOGGLE && i8253.bytetoggle[portnum] == 0)) {
                highbyte = 0;
            }
            else {
                if (i8253.accessmode[portnum] == PIT_MODE_HIBYTE ||
                    (i8253.accessmode[portnum] == PIT_MODE_TOGGLE && i8253.bytetoggle[portnum] == 1)) {
                    highbyte = 1;
                }
            }
            if (i8253.accessmode[portnum] == 0 || i8253.accessmode[portnum] == PIT_MODE_TOGGLE) {
                i8253.bytetoggle[portnum] = (~i8253.bytetoggle[portnum]) & 1;
            }
            if (highbyte) { //high byte
                return ((uint8_t)(i8253.counter[portnum] >> 8));
            }
            else { //low byte
                return ((uint8_t) i8253.counter[portnum]);
            }
            break;
    }

    return 0;
}
