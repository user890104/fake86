/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* i8237.c: functions to emulate the Intel 8237 DMA controller.
   the Sound Blaster emulation functions rely on this! */

#include "fake86/config.h"
#include <stdint.h>
#include <stdio.h>
#include <memory.h>
#include "fake86/i8237.h"
#include "fake86/memory.h"
#include "fake86/ports.h"

struct i8237_chan_t {
    uint32_t page;
    uint32_t addr;
    uint32_t reload;
    uint32_t count;
    uint8_t direction;
    uint8_t autoinit;
    uint8_t writemode;
    uint8_t masked;
};

static struct i8237_chan_t i8237_chan[4];

static void out8237(uint16_t addr, uint8_t value);
static uint8_t in8237(uint16_t addr);

static uint8_t flipflop;

void i8237_init(void) {
    memset(i8237_chan, 0, sizeof(i8237_chan));

    ports_set_write_redirector(0x00, 0x0F, &out8237);
    ports_set_read_redirector(0x00, 0x0F, &in8237);

    ports_set_write_redirector(0x80, 0x8F, &out8237);
    ports_set_read_redirector(0x80, 0x8F, &in8237);
}

uint8_t i8237_read(uint8_t channel) {
    uint8_t ret;
    if (i8237_chan[channel].masked) {
        return 128;
    }
    if (i8237_chan[channel].autoinit && (i8237_chan[channel].count > i8237_chan[channel].reload)) {
        i8237_chan[channel].count = 0;
    }
    if (i8237_chan[channel].count > i8237_chan[channel].reload) {
        return 128;
    }
    //if (i8237_chan[channel].direction) ret = memory_read8_direct(i8237_chan[channel].page + i8237_chan[channel].addr + i8237_chan[channel].count);
    //	else ret = memory_read8_direct(i8237_chan[channel].page + i8237_chan[channel].addr - i8237_chan[channel].count);
    if (i8237_chan[channel].direction == 0) {
        ret = memory_read8_direct(i8237_chan[channel].page + i8237_chan[channel].addr + i8237_chan[channel].count);
    }
    else {
        ret = memory_read8_direct(i8237_chan[channel].page + i8237_chan[channel].addr - i8237_chan[channel].count);
    }
    i8237_chan[channel].count++;
    return ret;
}

static void out8237(uint16_t addr, uint8_t value) {
    uint8_t channel;
#ifdef DEBUG_DMA
    printf ("out8237(0x%X, %X);\n", addr, value);
#endif
    switch (addr) {
        case 0x2: //channel 1 address register
            if (flipflop == 1) {
                i8237_chan[1].addr = (i8237_chan[1].addr & 0x00FF) | ((uint32_t) value << 8);
            }
            else {
                i8237_chan[1].addr = (i8237_chan[1].addr & 0xFF00) | value;
            }
#ifdef DEBUG_DMA
            if (flipflop == 1) printf ("[NOTICE] DMA channel 1 address register = %04X\n", i8237_chan[1].addr);
#endif
            flipflop = ~flipflop & 1;
            break;
        case 0x3: //channel 1 count register
            if (flipflop == 1) {
                i8237_chan[1].reload = (i8237_chan[1].reload & 0x00FF) | ((uint32_t) value << 8);
            }
            else {
                i8237_chan[1].reload = (i8237_chan[1].reload & 0xFF00) | value;
            }
            if (flipflop == 1) {
                if (i8237_chan[1].reload == 0) {
                    i8237_chan[1].reload = 65536;
                }
                i8237_chan[1].count = 0;
#ifdef DEBUG_DMA
                printf ("[NOTICE] DMA channel 1 reload register = %04X\n", i8237_chan[1].reload);
#endif
            }
            flipflop = ~flipflop & 1;
            break;
        case 0xA: //write single mask register
            channel = value & 3;
            i8237_chan[channel].masked = (value >> 2) & 1;
#ifdef DEBUG_DMA
            printf ("[NOTICE] DMA channel %u masking = %u\n", channel, i8237_chan[channel].masked);
#endif
            break;
        case 0xB: //write mode register
            channel = value & 3;
            i8237_chan[channel].direction = (value >> 5) & 1;
            i8237_chan[channel].autoinit = (value >> 4) & 1;
            i8237_chan[channel].writemode = (value >> 2) & 1; //not quite accurate
#ifdef DEBUG_DMA
            printf ("[NOTICE] DMA channel %u write mode reg: direction = %u, autoinit = %u, write mode = %u\n",
                    channel, i8237_chan[channel].direction, i8237_chan[channel].autoinit, i8237_chan[channel].writemode);
#endif
            break;
        case 0xC: //clear byte pointer flip-flop
#ifdef DEBUG_DMA
            printf ("[NOTICE] DMA cleared byte pointer flip-flop\n");
#endif
            flipflop = 0;
            break;
        case 0x83: //DMA channel 1 page register
            i8237_chan[1].page = (uint32_t) value << 16;
#ifdef DEBUG_DMA
            printf ("[NOTICE] DMA channel 1 page base = %05X\n", i8237_chan[1].page);
#endif
            break;
    }
}

static uint8_t in8237(uint16_t addr) {
#ifdef DEBUG_DMA
    printf ("in8237(0x%X);\n", addr);
#endif
    switch (addr) {
        case 3:
            if (flipflop == 1) {
                return (i8237_chan[1].reload >> 8);
            }
            else {
                return (i8237_chan[1].reload);
            }
            flipflop = ~flipflop & 1;
            break;
    }
    return (0);
}
