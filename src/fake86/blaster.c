/*
  Fake86: A portable, open-source 8086 PC emulator.
  Copyright (C)2010-2013 Mike Chambers

  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

/* blaster.c: functions to emulate a Creative Labs Sound Blaster Pro. */

#include "fake86/config.h"
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "fake86/adlib.h"
#include "fake86/blaster.h"
#include "fake86/i8259.h"
#include "fake86/i8237.h"
#include "fake86/platform.h"
#include "fake86/ports.h"

struct mixer_t {
    uint8_t index;
    uint8_t reg[256];
};

struct blaster_t {
    uint8_t mem[1024];
    uint16_t memptr;
    uint16_t samplerate;
    uint8_t dspmaj;
    uint8_t dspmin;
    uint8_t speakerstate;
    uint8_t lastresetval;
    uint8_t lastcmdval;
    uint8_t lasttestval;
    uint8_t waitforarg;
    uint8_t paused8;
    uint8_t paused16;
    uint8_t sample;
    uint8_t sbirq;
    uint8_t sbdma;
    uint8_t usingdma;
    uint8_t maskdma;
    uint8_t useautoinit;
    uint32_t blocksize;
    uint32_t blockstep;
    uint64_t sampleticks;
    struct mixer_t mixer;
};

static void bufNewData(uint8_t value);

static void setsampleticks(void);

static void cmdBlaster(uint8_t value);

static void outBlaster(uint16_t portnum, uint8_t value);

static uint8_t inBlaster(uint16_t portnum);

static void mixerReset(void);

static struct blaster_t blaster;
static uint8_t mixer[256], mixerindex;

#ifdef DEBUG_BLASTER
    static uint16_t lastread;
#endif

#ifdef DEBUG_BLASTER_FILE
static FILE *sbout = NULL;
#endif

void blaster_init(uint16_t baseport, uint8_t irq) {
#ifdef DEBUG_BLASTER_FILE
    sbout = fopen("sbout.raw", "wb");
#endif
    memset(&blaster, 0, sizeof(blaster));
    blaster.dspmaj = 2; //emulate a Sound Blaster 2.0
    blaster.dspmin = 0;
    blaster.sbirq = irq;
    blaster.sbdma = 1;
    mixerReset();
    ports_set_write_redirector(baseport, baseport + 0xE, &outBlaster);
    ports_set_read_redirector(baseport, baseport + 0xE, &inBlaster);
}

int16_t blaster_get_sample(void) {
    if (blaster.speakerstate == 0) {
        return (0);
    }
    else {
        return ((int16_t) blaster.sample - 128);
    }
}

void blaster_tick(void) {
    if (!blaster.usingdma) {
        return;
    }
    /*if (blaster.paused8) {
    blaster.sample = 128;
    return;
    }*/
    //printf("blaster_tick();\n");
    blaster.sample = i8237_read(blaster.sbdma);
#ifdef DEBUG_BLASTER_FILE
    if (sbout != NULL) fwrite(&blaster.sample, 1, 1, sbout);
#endif
    blaster.blockstep++;
    if (blaster.blockstep > blaster.blocksize) {
        i8259_do_irq(blaster.sbirq);
#ifdef DEBUG_BLASTER
        printf ("[NOTICE] Sound Blaster did IRQ\n");
#endif
        if (blaster.useautoinit) {
            blaster.blockstep = 0;
        }
        else {
            blaster.usingdma = 0;
        }
    }
}

uint16_t blaster_get_sample_rate(void) {
    return blaster.samplerate;
}

uint64_t blaster_get_sample_ticks(void) {
    return blaster.sampleticks;
}

static void outBlaster(uint16_t portnum, uint8_t value) {
#ifdef DEBUG_BLASTER
    printf ("[DEBUG] outBlaster: port %Xh, value %02X\n", portnum, value);
#endif
    portnum &= 0xF;
    // on the Sound Blaster Pro, ports (base+0) and (base+1) are for
    // the OPL FM music chips, and are also mirrored at (base+8) (base+9)
    // as well as 0x388 and 0x389 to remain compatible with the older adlib cards
    switch (portnum) {
        case 0x0:
        case 0x8:
            adlib_out(0x388, value);
            break;
        case 0x1:
        case 0x9:
            adlib_out(0x389, value);
            break;
        case 0x4: //mixer address port
            mixerindex = value;
            break;
        case 0x5: //mixer data
            mixer[mixerindex] = value;
            break;
        case 0x6: //reset port
            if ((value == 0x00) && (blaster.lastresetval == 0x01)) {
                blaster.speakerstate = 0;
                blaster.sample = 128;
                blaster.waitforarg = 0;
                blaster.memptr = 0;
                blaster.usingdma = 0;
                blaster.blocksize = 65535;
                blaster.blockstep = 0;
                bufNewData(0xAA);
                memset(mixer, 0xEE, sizeof(mixer));
#ifdef DEBUG_BLASTER
                printf ("[DEBUG] Sound Blaster received reset!\n");
#endif
            }
            blaster.lastresetval = value;
            break;
        case 0xC: //write command/data
            cmdBlaster(value);
            if (blaster.waitforarg != 3) {
                blaster.lastcmdval = value;
            }
            break;
    }
}

static uint8_t inBlaster(uint16_t portnum) {
    uint8_t ret;
#ifdef DEBUG_BLASTER
    if (lastread != portnum) {
        printf("[DEBUG] inBlaster: port %Xh, value ", portnum);
    }
#endif
    portnum &= 0xF;
    // on the Sound Blaster Pro, ports (base+0) and (base+1) are for
    // the OPL FM music chips, and are also mirrored at (base+8) (base+9)
    // as well as 0x388 and 0x389 to remain compatible with the older adlib cards
    switch (portnum) {
        case 0x0:
        case 0x8:
            ret = adlib_in(0x388);
            break;
        case 0x1:
        case 0x9:
            ret = adlib_in(0x389);
            break;
        case 0x5: //mixer data
            ret = mixer[mixerindex];
            break;
        case 0xA: //read data
            if (blaster.memptr == 0) {
                ret = 0;
            }
            else {
                ret = blaster.mem[0];
                memmove(&blaster.mem[0], &blaster.mem[1], sizeof(blaster.mem) - 1);
                blaster.memptr--;
            }
            break;
        case 0xE: //read-buffer status
            if (blaster.memptr > 0) {
                ret = 0x80;
            }
            else {
                ret = 0x00;
            }
            break;
        default:
            ret = 0x00;
    }
#ifdef DEBUG_BLASTER
    if (lastread != portnum) {
        printf ("%02X\n", ret);
    }
    lastread = portnum;
#endif
    return ret;
}

static void cmdBlaster(uint8_t value) {
    uint8_t recognized = 1;
    if (blaster.waitforarg) {
        switch (blaster.lastcmdval) {
            case 0x10: //direct 8-bit sample output
                blaster.sample = value;
                break;
            case 0x14: //8-bit single block DMA output
            case 0x24:
            case 0x91:
                if (blaster.waitforarg == 2) {
                    blaster.blocksize = (blaster.blocksize & 0xFF00) | (uint32_t) value;
                    blaster.waitforarg = 3;
                    return;
                }
                else {
                    blaster.blocksize = (blaster.blocksize & 0x00FF) | ((uint32_t) value << 8);
#ifdef DEBUG_BLASTER
                    printf ("[NOTICE] Sound Blaster DSP block transfer size set to %u\n", blaster.blocksize);
#endif
                    blaster.usingdma = 1;
                    blaster.blockstep = 0;
                    blaster.useautoinit = 0;
                    blaster.paused8 = 0;
                    blaster.speakerstate = 1;
                }
                break;
            case 0x40: //set time constant
                blaster.samplerate = (uint16_t)((uint32_t) 1000000 / (uint32_t)(256 - (uint32_t) value));
                setsampleticks();
#ifdef DEBUG_BLASTER
                printf ("[DEBUG] Sound Blaster time constant received, sample rate = %u\n", blaster.samplerate);
#endif
                break;
            case 0x48: //set DSP block transfer size
                if (blaster.waitforarg == 2) {
                    blaster.blocksize = (blaster.blocksize & 0xFF00) | (uint32_t) value;
                    blaster.waitforarg = 3;
                    return;
                }
                else {
                    blaster.blocksize = (blaster.blocksize & 0x00FF) | ((uint32_t) value << 8);
                    //if (blaster.blocksize == 0) blaster.blocksize = 65536;
                    blaster.blockstep = 0;
#ifdef DEBUG_BLASTER
                    printf ("[NOTICE] Sound Blaster DSP block transfer size set to %u\n", blaster.blocksize);
#endif
                }
                break;
            case 0xE0: //DSP identification for Sound Blaster 2.0 and newer (invert each bit and put in read buffer)
                bufNewData(~value);
                break;
            case 0xE4: //DSP write test, put data value into read buffer
                bufNewData(value);
                blaster.lasttestval = value;
                break;
            default:
                recognized = 0;
        }
        //blaster.waitforarg--; // = 0;
        if (recognized) {
            return;
        }
    }

    switch (value) {
        case 0x10:
        case 0x40:
        case 0xE0:
        case 0xE4:
            blaster.waitforarg = 1;
            break;

        case 0x14: //8-bit single block DMA output
        case 0x24:
        case 0x48:
        case 0x91:
            blaster.waitforarg = 2;
            break;

        case 0x1C: //8-bit auto-init DMA output
        case 0x2C:
            blaster.usingdma = 1;
            blaster.blockstep = 0;
            blaster.useautoinit = 1;
            blaster.paused8 = 0;
            blaster.speakerstate = 1;
            break;

        case 0xD0: //pause 8-bit DMA I/O
            blaster.paused8 = 1;
            break;
        case 0xD1: //speaker output on
            blaster.speakerstate = 1;
            break;
        case 0xD3: //speaker output off
            blaster.speakerstate = 0;
            break;
        case 0xD4: //continue 8-bit DMA I/O
            blaster.paused8 = 0;
            break;
        case 0xD8: //get speaker status
            if (blaster.speakerstate) {
                bufNewData(0xFF);
            }
            else {
                bufNewData(0x00);
            }
            break;
        case 0xDA: //exit 8-bit auto-init DMA I/O mode
            blaster.usingdma = 0;
            break;
        case 0xE1: //get DSP version info
            blaster.memptr = 0;
            bufNewData(blaster.dspmaj);
            bufNewData(blaster.dspmin);
            break;
        case 0xE8: //DSP read test
            blaster.memptr = 0;
            bufNewData(blaster.lasttestval);
            break;
        case 0xF2: //force 8-bit IRQ
            i8259_do_irq(blaster.sbirq);
            break;
        case 0xF8: //undocumented command, clears in-buffer and inserts a null byte
            blaster.memptr = 0;
            bufNewData(0);
            break;
        default:
            printf("[NOTICE] Sound Blaster received unhandled command %02Xh\n", value);
            break;
    }
}

static void mixerReset(void) {
    memset(blaster.mixer.reg, 0, sizeof(blaster.mixer.reg));
    blaster.mixer.reg[0x22] = blaster.mixer.reg[0x26] = blaster.mixer.reg[0x04] = (4 << 5) | (4 << 1);
}

static void bufNewData(uint8_t value) {
    if (blaster.memptr >= sizeof(blaster.mem)) {
        return;
    }
    blaster.mem[blaster.memptr] = value;
    blaster.memptr++;
}

static void setsampleticks(void) {
    if (blaster.samplerate == 0) {
        blaster.sampleticks = 0;
        return;
    }
    blaster.sampleticks = platform_get_host_freq() / (uint64_t) blaster.samplerate;
}
