DEBUG ?= 0
VERBOSE ?= 0

ifeq ($(DEBUG), 1)
    CFLAGS=-O0 -ggdb
    LDFLAGS=
else
    CFLAGS=-O3 -ffunction-sections -fdata-sections
    ifeq ($(VERBOSE), 1)
        LDFLAGS=-Wl,--gc-sections,--print-gc-sections
    else
        LDFLAGS=-Wl,--gc-sections
    endif
endif

SRCFILES=$(wildcard src/fake86/*.c)
OBJFILES=$(patsubst src/fake86/%.c,bin/%.o,$(SRCFILES))
BINPATH=/usr/bin
DATAPATH=/usr/share/fake86
CFLAGS+=-DPATH_DATAFILES=\"$(DATAPATH)/\" -Wall -Wextra -Wpedantic
INCLUDE=-Iinclude
LIBS=

UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	LIBS += -lX11
endif

SDL_CFLAGS=$(shell sdl-config --cflags)
SDL_LIBS=$(shell sdl-config --libs)

all: bin/fake86 bin/imagegen

bin/fake86: $(OBJFILES)
	$(CC) $^ -o $@ $(LDFLAGS) $(LIBS) $(SDL_LIBS)
	chmod a+x bin/fake86

bin/imagegen: src/imagegen/imagegen.c
	$(CC) -o bin/imagegen $(CFLAGS) src/imagegen/imagegen.c
	chmod a+x bin/imagegen

bin/%.o: src/fake86/%.c
	$(CC) -c $< -o $@ $(CFLAGS) $(INCLUDE) $(SDL_CFLAGS)

install:
	mkdir -p $(BINPATH)
	mkdir -p $(DATAPATH)
	chmod a-x data/*
	cp -p bin/fake86 $(BINPATH)
	cp -p bin/imagegen $(BINPATH)
	cp -p data/asciivga.dat $(DATAPATH)
	cp -p data/pcxtbios.bin $(DATAPATH)
	cp -p data/videorom.bin $(DATAPATH)
	cp -p data/rombasic.bin $(DATAPATH)

clean:
	rm -f src/fake86/*~
	rm -f src/imagegen/*~
	rm -f bin/*.o
	rm -f bin/fake86
	rm -f bin/imagegen

uninstall:
	rm -f $(BINPATH)/fake86
	rm -f $(BINPATH)/imagegen
	rm -f $(DATAPATH)/asciivga.dat
	rm -f $(DATAPATH)/pcxtbios.bin
	rm -f $(DATAPATH)/videorom.bin
	rm -f $(DATAPATH)/rombasic.bin
	rmdir $(DATAPATH)

.PHONY: all install clean uninstall
